module.exports = {
  dest: 'public',
  themeConfig: {
    search: false
  },
  plugins: [
    ['@vuepress/google-analytics', {'ga': 'UA-158305494-1'}]
  ]
}
