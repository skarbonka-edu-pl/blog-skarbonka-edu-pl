FROM node:12

ENV MODE=dev

WORKDIR /app

RUN yarn global add vuepress@1.3.0

CMD vuepress ${MODE} --debug src

EXPOSE 8080